import * as dotenv from "dotenv";
import * as http from "http";
import * as fs from "fs";
dotenv.config();

const homePage = fs.readFileSync("src/pages/home.html");
const aboutPage = fs.readFileSync("src/pages/about.html");
const notFoundPage = fs.readFileSync("src/pages/not-found.html");

const server = http.createServer((req, res) => {
  switch (req.url) {
    case "/":
      res.write(homePage);
      res.end("Homepage");
      break;
    case "/about":
      res.write(aboutPage);
      res.end("About");
      break;
    default:
      res.write(notFoundPage);
      res.end("404 Not Found");
      break;
  }
});

server.listen(process.env.PORT || 3000, () => {
  console.log("Listening on port " + process.env.PORT + " ...");
});
